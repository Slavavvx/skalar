<?php
echo getenv('TEST');

// Route
$method = $_SERVER["REQUEST_METHOD"];
$httpProtocol = $_SERVER["SERVER_PROTOCOL"];
$requestUri = $_SERVER["REQUEST_URI"];
$badRequest = '400 Bad Request';
$notFound = '404 Not Found';
$unsupport = '415 Unsupported Media Type';

$route = '';

$endpoints = [
	'\/'         => 'index.php',
	'\/logs\/.*' => './src/countingTime.php',
];

if ($method != 'GET' && $httpProtocol != 'HTTP/1.1') {

	header($httpProtocol .  ' ' . $badRequest);
	require_once 'error400.php';
	exit;
}

foreach ($endpoints as $key => $endpoint) {

	if (preg_match("#$key#", $requestUri)) {
		$route = $endpoint;
		break;
	}
}

if (empty($route)) {

	header($httpProtocol .  ' ' . $notFound);
	require_once 'error404.php';
	exit;
}

require_once $route;




