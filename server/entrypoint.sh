#!/usr/local/bin/php
<?php
    
    if (!isset($argv[1]) || !isset($argv[2])) {
         echo "You need to enter a host name and a port number 8890-8894 \n";
         exit;
    }

    $host = $argv[1];
    $port = $argv[2];
    $docRoot = '/var/www/slava/';
    $routePath = 'route.php';

    # $routePath = getenv('ROUTE_PATH');
    $startPort = getenv('START_PORT');
    $endPort = getenv('END_PORT');

    echo $routePath;

    if ($port >= $startPort && $port <= $endPort) {
        system(sprintf('php -S %s:%s %s', $host, $port, $routePath));
    } else {
        echo "Bad port number. Try again. \n";
    }

    # if ($port >= 8890 && $port <= 8894) {
    #     system(sprintf('php -S %s:%s -t %s', $host, $port, $docRoot));
    # } else {
    #     echo "Bad port number. Try again. \n";
    # }


    # Можно использовать команды Linux
    # echo system('ls');
