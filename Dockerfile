FROM php:8.1

ENV APP=/app

WORKDIR ${APP}

# WORKDIR /app

COPY server/ ./server
COPY log/ ${LOGFILE_PATH}

RUN chmod +x ./server/entrypoint.sh

ENTRYPOINT ["./server/entrypoint.sh"]

CMD [ "0.0.0.0", "8890" ]

EXPOSE 8890

COPY src/ ./src