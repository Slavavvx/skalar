<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Total time</title>
</head>
<body>
	<h3 style="color: orange">Total time of execution all requests is:</h3>
	<p style="font-weight: bold; color: grey; font-size: 18px;"><?= $totalTime ?></p>        
</body>
</html>