<?php

// Counting time execution of the all requests

$fileName = basename($requestUri);

$ext = pathinfo($fileName, PATHINFO_EXTENSION);

if ($ext != 'log') {

	header($httpProtocol .  ' ' . $unsupport);
	require_once 'error415.php';
	exit;
}

$path = getenv('LOGFILE_PATH');

$logfilePath = $path . $fileName;

if (!file_exists($logfilePath)) {

	header($httpProtocol .  ' ' . $notFound);
	require_once 'error404.php';
	exit;
}

$handle = fopen($path, 'r');

if (!$handle) {
	echo "It is not managed to open file!";
	exit;
}

$regex = 'resptime:\K"(([0-9]+)|([0-9]+\.[0-9]{1,4},?\s?)+)"';
$noSearch = 'resptime:"-"';
$search = 'resptime:';

$totalTime = 0;

while (!feof($handle)) {

	$row = fgets($handle);

    // if (strpos($row, $noSearch) === false) {

    // 	$pos = strpos($row, $search) + strlen($search);
    // 	$strTime = trim(substr($row, $pos), '"');
    // 	$time = explode(', ', $strTime);
    // 	$totalTime += array_sum($time);
    // }

    if (preg_match("#$regex#", $row, $matches)) {
		
		$strTime = trim($matches[1], '"');
		$time = explode(', ', $matches[1]);
    	$totalTime += array_sum($time);
	}
}

fclose($handle);

require_once 'totalTime.php';
