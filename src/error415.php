<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Unsupported Media Type</title>
</head>
<body>
	<h1 style="color: red;">Oops!</h1>
	<h3>Unsupported Media Type</h3>
	<p><span style="font-weight: bold; color: grey; font-size: 18px;">415</span> Unsupported Media Type</p>        
</body>
</html>