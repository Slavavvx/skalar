<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>404 Not Found</title>
</head>
<body>
	<h1 style="color: red;">Oops!</h1>
	<h3>You are lost...</h3>
	<p><span style="font-weight: bold; color: grey; font-size: 18px;">404</span> Page not found</p>        
</body>
</html>